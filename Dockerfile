FROM node:18-alpine

WORKDIR /app/back

COPY ./back/package*.json .

RUN npm ci

COPY ./back/ .
COPY ../types/ ../types/

RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]