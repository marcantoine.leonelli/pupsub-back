import { Socket } from "socket.io";
import { Message } from "@google-cloud/pubsub";
import { Message as AppMessage } from "../../../types/message.model";

export function handleSubMessage(socket: Socket, message: Message) {
  console.log("Received message:", message.data.toString());
  const messageReturn: AppMessage = {
    content: message.data.toString(),
    sender: JSON.parse(message.attributes.sender)
  }
  socket.emit("message", messageReturn);
  message.ack();
}
