
import { PubSub } from "@google-cloud/pubsub";
import credentials from "../leprojetsubv-03afe917999e.json";
import { User } from "../../types/user.model";

const TOPIC_NAME = "marc";
const SUBSCRIPTION_NAME = "marc-sub";

class PupsubClientBase {
  pubsub: any;
  topic: any;
  subscription: any;

  constructor() {
    this.pubsub = new PubSub({
      projectId: credentials.project_id,
      keyFilename: "./leprojetsubv-03afe917999e.json",
    });
    this.topic = this.pubsub.topic(TOPIC_NAME);
    this.subscription = this.topic.subscription(SUBSCRIPTION_NAME);
  }

  async publishMessage(content: string, sender: User) {
    this.topic.publishMessage({
      data: Buffer.from(content),
      attributes: {
        sender: JSON.stringify(sender),
      }
    })
  }

  async subscribe(callback: any) {
    this.subscription.on("message", callback);
  }

  async subscribeError(callback: any) {
    this.subscription.on("error", (error: any) => {
      console.error("Received error:", error);
      callback(error);
    });
  }
}

export default class PupsubClient {
  static instance: PupsubClientBase;

  static getInstance(): PupsubClientBase {
    if (!PupsubClient.instance) {
      PupsubClient.instance = new PupsubClientBase();
    }
    return PupsubClient.instance;
  }
}
