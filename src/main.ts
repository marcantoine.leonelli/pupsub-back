import { User } from "../../types/user.model";
import PupsubClient from "./PubsubClient";
import express from "express";
import { Server } from "socket.io";
import { createServer } from "node:http";
import cors from "cors";
import { handleSubMessage } from "./messages/message.controller";

const app = express();
const port = 8080;
const server = createServer(app);
const io = new Server(server, {
  cors: {
    origin: "*",
    credentials: false,
  },
});

app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello, world!");
});

app.post("/publish", (req, res) => {
  const client = PupsubClient.getInstance();

  const user: User = {
    name: "Marco",
    id: "1",
  };

  client.publishMessage("Hello, world!", user);

  res.send("Message published");
});

io.on("connection", (socket) => {
  const client = PupsubClient.getInstance();

  client.subscribe((message: any) => {
    handleSubMessage(socket, message);
  });

  socket.on("message", (message) => {
    console.log("Message:", message,);
    client.publishMessage(message.content, message.sender);
  });
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

server.listen(3000, () => {
  console.log("Socket server is running on http://localhost:3000");
});

